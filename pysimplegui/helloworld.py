'''
My first app
Important notes:
- using keys on items, returns a dictionary (awesome!) of (only) the items with a key;
- one-event & event loops possible. For event loop just add a loop. ;)
'''
#import PySimpleGUI as sg
#import PySimpleGUIQt as sg
import PySimpleGUIWeb as sg
#import PySimpleGUIKivy as sg


def create_layout():
    # ------ Menu Definition ------ #      
    menu_def = [['File', ['Open', 'Save', 'Exit', 'Properties']],      
                ['Edit', ['Paste', ['Special', 'Normal', ], 'Undo'], ],      
                ['Help', 'About...'], ]

    frame_layout = [
        [sg.Text("frame content")]
    ]

    # pane_list = []

    layout = [
        [sg.Menu(menu_def, tearoff=True)],
        # [sg.Pane(pane_list)],
        [sg.Frame("frame!", frame_layout)],
        [sg.Text("yo!", size=(50, 1))],
        [sg.InputText(key='yoInput')],
        [sg.Submit(button_text="OK"), sg.Cancel(), sg.Button("Dialog")],
        # [sg.Exit()]
        [sg.Text('', key="outText")],
        #[sg.StatusBar("status", auto_size_text=False)]
    ]
    return layout

def main():
    layout = create_layout()
    window = sg.Window("1st!").Layout(layout)

    # run in event loop
    while True:
        event, values = window.Read()
        #return (event, values)
        #print(event, values)
        
        window.FindElement('outText').Update(values['yoInput'])

        if event is None or event == 'Cancel':
            break
        if event == 'Dialog':
            sg.Popup("yo!!!")

    window.Close()

if (__name__ == "__main__"):
    main()
