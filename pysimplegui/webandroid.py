'''
This is the simplest GUI code, used for testing on Android.
'''
#import PySimpleGUIWeb as sg
import PySimpleGUIKivy as sg

layout = [ [sg.Text('Hello World')], [sg.Button('Exit')]]
win = sg.Window('Window').Layout(layout)
e,v = win.Read()
print(e,v)

win.Close()
