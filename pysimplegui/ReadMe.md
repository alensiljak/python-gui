# PySimpleGUI

https://github.com/PySimpleGUI/PySimpleGUI

supports Windows, Linux, Web or rather TKinter, Qt, Kivy, Web.

## PySimpleGUIQt

Qt version requires the following modules: PySimpleGUIQt, PySide2.
It does not support StatusBar.

## PySimpleGUIWeb

Web version requires: pysimpleguiweb, remi.

This is the only version that also runs on Android.
When running web version, specify the port ("web_port") as a parameter to Window constructor.
To run both the client (web browser) and the server (app), see gnucash_portfolio_webui run.py.

## PySimpleGUIKivy

Requires: cython.

## Architecture

The whole application could, theoretically, fit into one Python file.
