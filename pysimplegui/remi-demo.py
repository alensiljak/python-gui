'''
Simple demo of web ui using remi.
'''
import PySimpleGUIWeb as sg


def create_layout():
    layout = [
        #[sg.Menu(menu_def, tearoff=True)],
        # [sg.Pane(pane_list)],
        #[sg.Frame("frame!", frame_layout)],
        [sg.Text("yo!", size=(50, 1))],
        [sg.InputText(key='yoInput')],
        [sg.Submit(button_text="OK"), sg.Cancel(), sg.Button("Dialog")],
        # [sg.Exit()]
        [sg.Text('', key="outText")],
        #[sg.StatusBar("status", auto_size_text=False)]
    ]
    return layout

def main():
    layout = create_layout()
    window = sg.Window("1st!",
        web_debug=True, 
        #web_start_browser=False
        web_port=8080
        ).Layout(layout)

    # run in event loop
    while True:
        event, values = window.Read()
        print(event, values)
        
        window.FindElement('outText').Update(values['yoInput'])

        if event is None or event == 'Cancel':
            break
        if event == 'Dialog':
            sg.Popup("yo!!!")

    window.Close()

if (__name__ == "__main__"):
    main()
