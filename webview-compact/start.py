#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a demo for [PyWebView](https://github.com/r0x0r/pywebview) using integrated Python code.
It also demonstrates calling Python code as a JavaScript API.
The HTML and JavaScript are in index.html.
"""
import webview
import threading
import time
import sys
import random


class Api:
    """ The API interface made available to the JavaScript """
    def __init__(self):
        self.cancel_heavy_stuff_flag = False

    def init(self, params):
        response = {
            'message': 'Hello from Python {0}'.format(sys.version)
        }
        return response

    def getRandomNumber(self, params):
        response = {
            'message': 'Here is a random number courtesy of randint: {0}'.format(random.randint(0, 100000000))
        }
        return response

    def doHeavyStuff(self, params):
        time.sleep(0.1)  # sleep to prevent from the ui thread from freezing for a moment
        now = time.time()
        self.cancel_heavy_stuff_flag = False
        for i in range(0, 1000000):
            _ = i * random.randint(0, 1000)
            if self.cancel_heavy_stuff_flag:
                response = {'message': 'Operation cancelled'}
                break
        else:
            then = time.time()
            response = {
                'message': 'Operation took {0:.1f} seconds on the thread {1}'.format((then - now), threading.current_thread())
            }
        return response

    def cancelHeavyStuff(self, params):
        time.sleep(0.1)
        self.cancel_heavy_stuff_flag = True


def create_app():
    # load HTML from a file
    with open("index.html", mode='r') as html_file:
        html = html_file.read()
    #webview.create_window("It works, Hamed!", "")
    webview.load_html(html)


if __name__ == '__main__':
    t = threading.Thread(target=create_app)
    t.start()

    api = Api()
    webview.create_window('API example', js_api=api, debug=True)
