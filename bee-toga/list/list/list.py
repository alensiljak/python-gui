"""
A more complex example, utilizing different UI elements 
Includes a browser example from https://toga.readthedocs.io/en/latest/tutorial/tutorial-3.html
"""

import toga
from toga.style.pack import Pack, ROW, CENTER, COLUMN


class ListApp(toga.App):
    """ App class """
    
    def button_handler(self, widget):
        """ Handle button click """
        #print("hello")
        self.label.text = "Hola!"

    def load_page(self, widget):
        self.webview.url = self.url_input.value

    def draw_gui(self):
        """ Create the UI elements """
        box = toga.Box(style=Pack(direction=COLUMN))
        # children=[self.label]

        button = toga.Button('Hello world', on_press=self.button_handler)
        button.style.padding = 50
        button.style.flex = 1
        box.add(button)

        # Label to show responses.
        self.label = toga.Label('Ready.')
        box.add(self.label)

        self.webview = toga.WebView(style=Pack(flex=1))
        box.add(self.webview)

        # URL
        url_box = toga.Box()

        self.url_input = toga.TextInput(
            initial='https://alensiljak.ml/',
            style=Pack(flex=1)
        )
        url_box.add(self.url_input)

        go_button = toga.Button(label="Go")
        go_button.on_press = self.load_page
        url_box.add(go_button)

        box.add(url_box)
        
        return box

    def init_menus(self):
        """ Set up menus """
        # Commands
        things = toga.Group('Things')
        cmd0 = toga.Command(
            self.load_page,
            label='Action 0',
            tooltip='Perform action 0',
            #icon=brutus_icon,
            icon = toga.Icon.TIBERIUS_ICON,
            group=things
        )
        cmd2 = toga.Command(
            self.load_page,
            label='Action 2',
            tooltip='Perform action 2',
            icon=toga.Icon.TIBERIUS_ICON,
            group=things
        )
        # Menus
        self.commands.add(cmd0)
        self.main_window.toolbar.add(cmd0, cmd2)

    def init_app(self):
        """ Custom initialization code """

        self.webview.url = self.url_input.value

    def startup(self):
        """ Startup """

        self.main_window = toga.MainWindow(title=self.name)

        box = self.draw_gui()
        self.init_menus()
        self.init_app()

        # Add the content on the main window
        self.main_window.content = box
        # Show the main window
        self.main_window.show()


def main():
    return ListApp('List App', 'ml.alensiljak.listapp') # startup=build


if __name__ == '__main__':
    app = main()
    app.main_loop()
