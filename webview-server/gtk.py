#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
https://pygobject.readthedocs.io/en/latest/getting_started.html
Testing if pygobject bindings work (python - gtk3).
"""
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

window = Gtk.Window(title="Hello World")
window.show()
window.connect("destroy", Gtk.main_quit)
Gtk.main()