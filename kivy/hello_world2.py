"""
This should be a bit more complex example, but still basic.
Loading of the UI definition from a .kv file.
"""
from kivy.app import App
from kivy.uix.button import Button

class HelloWorld2(App):
    # def build(self):
    #     return Button(text='Hello World')
    
    def button_pressed(self):
        print("click!")

HelloWorld2().run()
