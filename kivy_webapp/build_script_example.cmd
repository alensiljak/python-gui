:: 
:: ref: 
:: https://python-for-android.readthedocs.io/en/latest/quickstart/

p4a apk --private $HOME/code/myapp --package=org.example.myapp --name "My WebView Application" --version 0.1 --bootstrap=webview --requirements=flask --port=5000

:: it is better to use the .p4a configuration file.
:: --package=org.example.myapp