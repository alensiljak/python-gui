"""
This should run Flask server in a separate thread and open a web view that connects to it.

https://stackoverflow.com/questions/49884548/how-to-run-kivy-and-flask-apps-together
"""
import threading
import kivy
from kivy.app import App
from flask import Flask
import os
from kivy.uix.floatlayout import FloatLayout

kivy.require('1.10.1')
new_environ = os.environ.copy()

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World'
def start_app():
    print("Starting Flask app...")
    app.run(port=5000, debug=False)     #specify separate port to run Flask app


class Controller(FloatLayout):
    """ This is the UI controller """

    def button_pressed(self):
        """ handle button press """
        print("alive!")


class MyApp(App):

    def build(self):
        return Controller()


if __name__ == '__main__':
    # todo: uncomment these lines to run Flask in a separate thread.
    # if os.environ.get("WERKZEUG_RUN_MAIN") != 'true':
    #     threading.Thread(target=start_app).start()
    MyApp().run()