# NativeScript

To run the app use `tns run HelloWorld`.

## Links

- [Windows Setup](https://docs.nativescript.org/start/ns-setup-win)
- https://docs.nativescript.org/start/cli-basics#creating-apps
