# Python for Android

Part of Kivy project. Here we refer only to using the webserver option.

It (still) seems to run correctly only on Linux.

## Limitations

- python-for-android 
	- currently only runs on Linux
	- seems to require python2
- there are issues running Flask server in a separate thread on Android so this may not be possible at all

## Installation

Follow the installation instructions (see below).

On Windows, also run
`python -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew`

## WebView

WebView approach will start Flask web server in a separate thread and then run the app with a web view, connecting to the web server. This allows for the use of HTML/CSS/JavaScript technologies that can be reused for multiple platforms.

## Flask

Flask server should be run in a separate process and then the client connects to it

- https://stackoverflow.com/questions/49884548/how-to-run-kivy-and-flask-apps-together

### Resources

- [Android apps with Python, Flask and a WebView](http://inclem.net/2016/05/07/kivy/python_for_android_webview_support/)
- [webdebugger](https://github.com/kivy/kivy/blob/96dfa5b55ed74c8223152dd84cd478bfed03e90b/kivy/modules/webdebugger.py) module.
- [Use webview in kivy as a widget](https://groups.google.com/forum/#!topic/kivy-users/JRTFpQuwgpE)
- [Packaging for Android](https://kivy.org/doc/stable/guide/packaging-android.html)
- [Android](https://kivy.org/doc/stable/guide/android.html)
	- [Packaging your application for the Kivy Launcher](https://kivy.org/doc/stable/guide/packaging-android.html#packaging-your-application-for-kivy-launcher)
- Python-for-Android
	- [Quickstart](https://python-for-android.readthedocs.io/en/latest/quickstart/)
- [Android native embedded browser](https://github.com/kivy/kivy/wiki/Android-native-embedded-browser)