# Kivy

Kivy user python-for-android project to package the app for Android. 

There are two possible outputs:

- app using Kivy for rendering,
- app with WebClient and Flask web server

The app can also be packaged for Kivy launcher and not as an individual apk.

This doc deals only with Kivy libraries. For web view option see [python-for-android](python-for-android.md).

## Documentation

- [Kivy Installation](https://kivy.org/doc/stable/gettingstarted/installation.html)
	- [Windows](https://kivy.org/doc/stable/installation/installation-windows.html#install-win-dist)
