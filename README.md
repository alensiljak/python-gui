# python-gui

Experimenting with various options for multi-platform Python GUI.

Respective technology-related notes are in their own files in the Doc folder.

A few helpful resources:

- [Android page](https://wiki.python.org/moin/Android) on Python wiki;

## Options

See `doc` folder for available technologies.

## To-inspect

- [pyqtdeploy](https://www.riverbankcomputing.com/software/pyqtdeploy/intro)

## Desktop-only

These are not actively considered but are listed here for reference.

### Qt

Qt is a C++ UI library. `PyQt5` or `PySlide2` provide Python bindings. The UI designer (Qt Designer) can be used as a WYSIWYG editor for the forms.

The code gets translated into Python with a utility. The remaining logic is done in Python directly.

Package Qt app for different platforms: [qtdeploy](https://pypi.python.org/pypi/pyqtdeploy)

### GTK

The principle is the same as for Qt. This is a different UI library in C++.

`pygobject` or `python-gtk` Python bindings.

## Mobile-only

- [Chaquopy](https://chaquo.com/chaquopy/doc/current/python.html)
- [Native Script](https://www.nativescript.org/)
- [PhoneGap](www.phonegap.com)

## Resources

- https://wiki.python.org/moin/GuiProgramming
