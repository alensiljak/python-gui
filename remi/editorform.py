
# -*- coding: utf-8 -*-

import remi.gui as gui
from remi.gui import *
from remi import start, App


class CLASSexitButton( Button ):
    def __init__(self, *args):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        super( CLASSexitButton, self ).__init__(*args)
        

class CLASSdoSomethingButton( Button ):
    def __init__(self, *args):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        super( CLASSdoSomethingButton, self ).__init__(*args)
        

class CLASSrootContainer( Widget ):
    def __init__(self, *args):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        super( CLASSrootContainer, self ).__init__(*args)
        title = Label('Hello! Welcome to the GUI app in Python!')
        title.attributes.update({"class":"Label","editor_constructor":"('Hello! Welcome to the GUI app in Python!')","editor_varname":"title","editor_tag_type":"widget","editor_newclass":"False","editor_baseclass":"Label"})
        title.style.update({"margin":"0px","width":"302px","height":"33px","top":"34px","left":"158px","position":"absolute","overflow":"auto","justify-content":"center","align-items":"center","align-content":"center"})
        self.append(title,'title')
        exitButton = CLASSexitButton('Close')
        exitButton.attributes.update({"class":"Button","editor_constructor":"('Close')","editor_varname":"exitButton","editor_tag_type":"widget","editor_newclass":"True","editor_baseclass":"Button"})
        exitButton.style.update({"margin":"0px","width":"100px","height":"30px","top":"294px","left":"249px","position":"absolute","overflow":"auto"})
        self.append(exitButton,'exitButton')
        inputText = TextInput(False,'enter name')
        inputText.attributes.update({"class":"TextInput","placeholder":"enter name","autocomplete":"off","editor_constructor":"(False,'enter name')","editor_varname":"inputText","editor_tag_type":"widget","editor_newclass":"False","editor_baseclass":"TextInput","title":"Name Box"})
        inputText.style.update({"margin":"0px","width":"306px","height":"26px","top":"96px","left":"135px","position":"absolute","overflow":"auto"})
        self.append(inputText,'inputText')
        doSomethingButton = CLASSdoSomethingButton('Do Stuff')
        doSomethingButton.attributes.update({"class":"Button","editor_constructor":"('Do Stuff')","editor_varname":"doSomethingButton","editor_tag_type":"widget","editor_newclass":"True","editor_baseclass":"Button"})
        doSomethingButton.style.update({"margin":"0px","width":"100px","height":"30px","top":"96px","left":"458px","position":"absolute","overflow":"auto"})
        self.append(doSomethingButton,'doSomethingButton')
        singleLineText = TextInput(True,'just a single line')
        singleLineText.attributes.update({"class":"TextInput","rows":"1","placeholder":"just a single line","autocomplete":"off","editor_constructor":"(True,'just a single line')","editor_varname":"singleLineText","editor_tag_type":"widget","editor_newclass":"False","editor_baseclass":"TextInput"})
        singleLineText.style.update({"margin":"0px","resize":"none","width":"260px","height":"21px","top":"169px","left":"174px","position":"absolute","overflow":"auto"})
        self.append(singleLineText,'singleLineText')
        self.children['doSomethingButton'].onclick.do(self.onclick_doSomethingButton)
        
    def onclick_doSomethingButton(self, emitter):
        ''' Display entered text '''
        input = self.children['inputText']
        text = input.children["text"]
        print(text)


class MyApp(App):
    def __init__(self, *args, **kwargs):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        if not 'editing_mode' in kwargs.keys():
            super(MyApp, self).__init__(*args, static_file_path={'my_res':'./res/'})

    def idle(self):
        #idle function called every update cycle
        pass
    
    def main(self):
        return MyApp.construct_ui(self)
        
    @staticmethod
    def construct_ui(self):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        rootContainer = CLASSrootContainer()
        rootContainer.attributes.update({"class":"Widget","editor_constructor":"()","editor_varname":"rootContainer","editor_tag_type":"widget","editor_newclass":"True","editor_baseclass":"Widget"})
        rootContainer.style.update({"margin":"0px","width":"96%","height":"388px","top":"20px","left":"20px","position":"absolute","overflow":"auto"})
        rootContainer.children['exitButton'].onclick.do(self.onclick_exitButton)
        

        self.rootContainer = rootContainer
        return self.rootContainer
    
    def onclick_exitButton(self, emitter):
        self.close()


#Configuration
configuration = {'config_project_name': 'MyApp', 'config_address': '0.0.0.0', 'config_port': 8080, 'config_multiple_instance': True, 'config_enable_file_cache': True, 'config_start_browser': False, 'config_resourcepath': './res/'}

if __name__ == "__main__":
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    start(MyApp, address=configuration['config_address'], port=configuration['config_port'], 
                        multiple_instance=configuration['config_multiple_instance'], 
                        enable_file_cache=configuration['config_enable_file_cache'],
                        start_browser=configuration['config_start_browser'])
